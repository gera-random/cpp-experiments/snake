#include <SFML/Window.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/String.hpp>
#include <deque>
#include <random>
#include <iostream>
#include <sstream>

struct BoardRandomness {
	std::random_device dev;
	std::mt19937 rng;
	std::uniform_int_distribution<std::mt19937::result_type> dist_x;
	std::uniform_int_distribution<std::mt19937::result_type> dist_y;

	BoardRandomness(sf::Rect<int> board) : dist_x(board.left, board.left + board.width - 1),
										   dist_y(board.top, board.top + board.height - 1), rng(dev()) {}
};

struct Game {
	std::deque<sf::Vector2<int>> snake;
	sf::Vector2<int> snakeDirection;
	int growth;
	bool gameover;
	sf::Rect<int> board;
	BoardRandomness br;
	sf::Vector2<int> apple;
	int apples_eaten;
	bool generate_apple;

	Game() : board{0,0,64,36}, br{board}, apples_eaten{0}, generate_apple{true} {}
};

void drawSnake(Game &g, sf::RenderWindow &window)
{
	sf::RectangleShape shape(sf::Vector2f(1.f, 1.f));
	shape.setFillColor(sf::Color(150, 50, 250));
	for (sf::Vector2<int> &point : g.snake) {
		shape.setPosition(point.x, point.y);
		window.draw(shape);
	}
}

void drawApple(Game &g, sf::RenderWindow &window)
{
	sf::RectangleShape shape(sf::Vector2f(1.f, 1.f));
	shape.setPosition(g.apple.x, g.apple.y);
	shape.setFillColor(sf::Color(255, 0, 0));
	window.draw(shape);
}

bool checkGameOver(Game &g)
{
	sf::Vector2<int> head = g.snake[0];
	if (!g.board.contains(head)) return true;
	for (int i = 1; i < g.snake.size(); ++i)
		if (g.snake[i] == head)
			return true;
	return false;
}

bool newApplePos(Game &g)
{
	bool skip_this_frame = false;
	g.apple = {(int)g.br.dist_x(g.br.rng), (int)g.br.dist_y(g.br.rng)};
	for (int i = 0; i < g.snake.size(); ++i) {
		if (g.snake[i] == g.apple)
			return true;
	}
	return false;
}

void moveSnake(Game &g)
{
	g.snake.push_front({g.snake[0].x + g.snakeDirection.x, g.snake[0].y + g.snakeDirection.y});
	if (g.growth > 0)
		--g.growth;
	else
		g.snake.pop_back();
}

bool checkApple(Game &g)
{
	if (g.snake[0].x == g.apple.x && g.snake[0].y == g.apple.y) {
		g.growth += 3;
		return true;
	}
	return false;
}

void changeSnakeDirection(Game &g, const sf::Vector2<int> &newDirection)
{
	if (newDirection.x != -g.snakeDirection.x
	    && newDirection.y != -g.snakeDirection.y) g.snakeDirection = newDirection;
}

void reset(Game &g)
{
	g.snake = {{12, 10}, {11, 10}, {10, 10}};
	g.snakeDirection = {1, 0};
	g.gameover = false;
	g.growth = 0;
	g.generate_apple = true;
	g.apples_eaten = 0;
}		

void setWindowTitle(sf::Window &window, Game &g)
{
	sf::String windowTitle = "Snake game (";
	windowTitle += std::to_string(g.apples_eaten);
	windowTitle += " apples eaten)";

	window.setTitle(windowTitle);
}

sf::View getLetterboxView(sf::View view, int windowWidth, int windowHeight) {

    // Compares the aspect ratio of the window to the aspect ratio of the view,
    // and sets the view's viewport accordingly in order to achieve a letterbox effect.
    // A new view (with a new viewport set) is returned.

    float windowRatio = (float) windowWidth / (float) windowHeight;
    float viewRatio = view.getSize().x / (float) view.getSize().y;
    float sizeX = 1;
    float sizeY = 1;
    float posX = 0;
    float posY = 0;

    bool horizontalSpacing = true;
    if (windowRatio < viewRatio)
        horizontalSpacing = false;

    // If horizontalSpacing is true, the black bars will appear on the left and right side.
    // Otherwise, the black bars will appear on the top and bottom.

    if (horizontalSpacing) {
        sizeX = viewRatio / windowRatio;
        posX = (1 - sizeX) / 2.f;
    }

    else {
        sizeY = windowRatio / viewRatio;
        posY = (1 - sizeY) / 2.f;
    }

    view.setViewport( sf::FloatRect(posX, posY, sizeX, sizeY) );

    return view;
}

int main()
{
	Game g;
	reset(g);

	sf::RenderWindow window(sf::VideoMode(960, 540), "Snake game");

	// view for drawing score text
	sf::View textView({(float)window.getSize().x/2, (float)window.getSize().y/2,
			(float)window.getSize().x, (float)window.getSize().y});

	// view for drawing game board
	sf::View view({0, 0, (float)g.board.width, (float)g.board.height});
	window.setKeyRepeatEnabled(false);
	window.setFramerateLimit(12);

	sf::Font font;
	std::string fontFileName = "fonts/FiraCode-Bold.ttf";
	font.loadFromFile(fontFileName);

	while (window.isOpen()) {
		sf::Event event;
		while (true) {
			bool moreEvents = window.pollEvent(event);
			switch (event.type) {
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::Resized:
					textView.setSize(event.size.width, event.size.height);
					textView.setCenter(event.size.width/2, event.size.height/2);
					view = getLetterboxView(view, event.size.width, event.size.height);
					break;
				case sf::Event::KeyPressed:
					if (event.key.control && event.key.code == sf::Keyboard::Q)
						window.close();
					else if (event.key.code == sf::Keyboard::W) {
						changeSnakeDirection(g, {0, -1});

						// fix snake getting opposite direction when pressing keys fast
						moreEvents = false;
					}
					else if (event.key.code == sf::Keyboard::S) {
						changeSnakeDirection(g, {0, 1});
						moreEvents = false;
					}
					else if (event.key.code == sf::Keyboard::A) {
						changeSnakeDirection(g, {-1, 0});
						moreEvents = false;
					}
					else if (event.key.code == sf::Keyboard::D) {
						changeSnakeDirection(g, {1, 0});
						moreEvents = false;
					}
					else if (event.key.code == sf::Keyboard::Space && g.gameover) {
						reset(g);
						setWindowTitle(window, g);
					}
					break;
			}
			if (!moreEvents)
				break;
		}

		window.clear();
		window.setView(view);
		
		sf::RectangleShape shape(sf::Vector2f(g.board.width, g.board.height));
		shape.setFillColor(sf::Color(49, 20, 50));
		window.draw(shape);
		
		drawSnake(g, window);
		if (!g.generate_apple) drawApple(g, window);

		if (!g.gameover) {
			moveSnake(g);
			if (checkApple(g)) {
				++g.apples_eaten;
				setWindowTitle(window, g);
				g.generate_apple = true;
			}
		}
		g.gameover = checkGameOver(g);

		if (g.generate_apple)
			g.generate_apple = newApplePos(g);

		window.setView(textView);
		sf::Text text;
		text.setFont(font);
		std::stringstream ss;
		ss << g.apples_eaten;
		text.setString(ss.str());
		text.setCharacterSize(48);
		text.setFillColor(sf::Color(208, 86, 99));
		text.setPosition(0, window.getSize().y - 52);
		window.draw(text);

		window.display();
	}

	return 0;
}
