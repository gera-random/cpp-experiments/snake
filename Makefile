LDFLAGS = -lsfml-graphics -lsfml-window -lsfml-system 

all: snake

snake: main.cpp
	$(CXX) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	-rm -r snake
